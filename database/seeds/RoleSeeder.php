<?php

use Illuminate\Database\Seeder;
use App\RoleModel;
class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RoleModel::create(['name'=>'Administrator']);
        RoleModel::create(['name'=>'Client']);
    }
}

<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function test()
    {

        return response()->json(['success' => true, 'message' => 'You Are Logged In As Admin']);
    }
    public function add_user(Request $request)
    {
        $user = new User();
        $user->name=$request['name'];
        $user->email=$request['email'];
        $user->password=$request['password'];
        $user->save();
        return response()->json(['success'=>true,'message'=>'user  added']);
    }
    public function del_user($id)
    {
        User::where('id',$id)->delete();
        return response()->json(['success'=>true,'message'=>'user deleted']);
    }
}

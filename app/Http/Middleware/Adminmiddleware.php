<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class Adminmiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user=Auth::user()->role_id;

        if($user!=1){
            return response()->json(['success'=>false,'message'=>'You Are Not Allowed To Access This Area']);
        }
        return $next($request);
    }
}
